extends Timer

onready var cd_label = get_node("/root/World/GUI/COUNTDOWN")

func _process(delta):
	if time_left > 0:
		cd_label.set_text(String(ceil(time_left)))
	else:
		cd_label.set_text("")
