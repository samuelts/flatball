extends Area2D

func _ready():
	var BallManager = get_node("/root/World/BallManager")
	var ScoreKeeper = get_node("/root/World/ScoreKeeper")
	self.connect("body_entered", BallManager, "destroy_ball")
	self.connect("body_entered", ScoreKeeper, "player2_goal")