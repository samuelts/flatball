extends RigidBody2D

var thrust = Vector2(500,500)
var target = Vector2()
var thrust_scale = 1

func _ready():
	target = position
	
func _integrate_forces(state):
	var fx = int(Input.is_action_pressed("p2_right")) - int(Input.is_action_pressed("p2_left"))
	var fy = int(Input.is_action_pressed("p2_down")) - int(Input.is_action_pressed("p2_up"))
	var unit_force = Vector2(fx,fy).normalized()
	
	if Input.is_action_pressed("p2_thrust"):
		thrust_scale += 0.2
		thrust_scale = clamp(thrust_scale, 1, 3)
	else:
		thrust_scale = 1
	
	if Input.is_action_pressed("p2_brake"):
		linear_damp = 5
	else:
		linear_damp = 1
	
	set_applied_force(unit_force * thrust * thrust_scale)
	rotation = linear_velocity.angle() + PI/2
	
	if Input.is_action_just_pressed("p2_start"):
		get_tree().quit()
