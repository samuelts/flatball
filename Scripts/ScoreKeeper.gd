extends Node2D

var player1_score = 0
var player2_score = 0
export var delay = 5

onready var p1_label = get_node("/root/World/GUI/P1_SCORE")
onready var p2_label = get_node("/root/World/GUI/P2_SCORE")
onready var title_label = get_node("/root/World/GUI/TITLE")
onready var timer = get_node("./ScoreTimer")

func player1_goal(body):
	if body.is_in_group("Balls"):
		print("Player 1 Scored")
		player1_score += 1
		p1_label.set_text(String(player1_score))
		title_label.set_text("PLAYER 1  SCORES!!!")
		timer.connect("timeout", self, "reset_title")
		timer.set_wait_time(delay)
		timer.start()
		

func player2_goal(body):
	if body.is_in_group("Balls"):
		print("Player 2 Scored")
		player2_score += 1
		p2_label.set_text(String(player2_score))
		title_label.set_text("PLAYER 2  SCORES!!!")
		timer.connect("timeout", self, "reset_title")
		timer.set_wait_time(delay)
		timer.start()
		
func reset_title():
	timer.stop()
	title_label.set_text("FLAT  BALL")