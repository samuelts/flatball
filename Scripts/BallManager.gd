extends Node2D

onready var timer = get_node("./BallTimer")
export var delay = 5
export var origin = Vector2(960,540)
export (PackedScene) var newBall

func destroy_ball(body):
	if body.is_in_group("Balls"):
		body.queue_free()
		timer.connect("timeout", self, "create_ball")
		timer.set_wait_time(delay)
		timer.start()
		
func create_ball():
	timer.stop()
	var new_ball = newBall.instance()
	new_ball.name = str(get_tree().get_network_unique_id())
	new_ball.set_network_master(get_tree().get_network_unique_id())
	new_ball.add_to_group("Balls")
	add_child(new_ball)
	new_ball.ball_init()
