extends RigidBody2D

var online = Network.online
var ball_origin = Vector2(960,540)
var savedTrans = null

slave func updateTrans(t):
	savedTrans = t
	
func ball_init():
	set_position(ball_origin)
	
func _ready():
	self.add_to_group("Balls")

func _integrate_forces(state):
	if online:
		if is_network_master():
			rpc("updateTrans", state.transform)
		else:
			if (savedTrans != null):
				state.transform = savedTrans