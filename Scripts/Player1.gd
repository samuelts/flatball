extends KinematicBody2D

const FLOOR_NORM = Vector2(0,0)
const SLOPE_STOP_MIN = 25.0
const WALK_SPEED = 800
var velocity = Vector2()
var lastvel = Vector2()
var aim = Vector2()
var boost = 4
var boostCount = 60
var boostCool = 90
var target_hspeed = 0
var target_vspeed = 0
var target_hrot = 0
var target_vrot = 0
var target = 0
var move = false
var pushed_body = null
var pushed_vel = null
var collision = null

func _ready():
	target = position

func _get_input():
	target_hspeed = 0
	target_vspeed = 0
	target_hrot = 0
	target_vrot = 0
	
	#Movement Input
	if Input.is_action_pressed("move_left"):
		target_hspeed -= 1
	if Input.is_action_pressed("move_right"):
		target_hspeed +=  1
	if Input.is_action_pressed("move_up"):
		target_vspeed -= 1
	if Input.is_action_pressed("move_down"):
		target_vspeed += 1
		
	#Rotation Input
	if Input.is_action_pressed("aim_left"):
		target_hrot -= 1
	if Input.is_action_pressed("aim_right"):
		target_hrot +=  1
	if Input.is_action_pressed("aim_up"):
		target_vrot -= 1
	if Input.is_action_pressed("aim_down"):
		target_vrot += 1
		

func _physics_process(delta):
	_get_input()
	#velocity.x = lerp(velocity.x, target_hspeed*WALK_SPEED, 0.05)
	#velocity.y = lerp(velocity.y, target_vspeed*WALK_SPEED, 0.05)
	
	aim.x = lerp(aim.x, target_hrot*WALK_SPEED, 0.1)
	aim.y = lerp(aim.y, target_vrot*WALK_SPEED, 0.1)
	
	if Input.is_action_pressed("mb_left"):
		target = get_global_mouse_position()
		rotation = (target-position).angle() + PI/2
		
	if Input.is_action_just_released("mb_left"):
		move = true
	
	if move:
		velocity = (target - position).normalized() * WALK_SPEED

		if (target - position).length() > 15:
			velocity = move_and_slide(velocity, FLOOR_NORM, SLOPE_STOP_MIN)
		else:
			move = false

	collision = get_slide_collision(0)
	
	if collision:
		collision.collider.apply_impulse(Vector2(), velocity/100)
		