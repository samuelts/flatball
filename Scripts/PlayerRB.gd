extends RigidBody2D

var online = Network.online
var move_left = ""
var move_right = ""
var move_up = ""
var move_down = ""
var boost = ""
var brake = ""
var start = ""
var player1_origin = Vector2(660,540)
var player2_origin = Vector2(1260,540)

onready var player1_texture = load("res://Assets/player1.svg")
onready var player2_texture = load("res://Assets/player2.svg")
onready var sprite = get_node("PlayerSprite")

var thrust = Vector2(700,700)
var boost_scale = 500

slave var slave_position = Vector2()
slave var slave_rotation = 0.0
var savedTrans = null
var savedState = null

slave func updateTrans(t):
	savedTrans = t

slave func get_state(t):
	savedState = t

func init(is_slave):
	if get_tree().is_network_server():
		player1_init()
	else:
		player1_init()
		set_position(player2_origin)
		
	if is_slave:
		set_texture(player2_texture)
		
func player1_init():
	move_left = "p1_left"
	move_right = "p1_right"
	move_up = "p1_up"
	move_down = "p1_down"
	boost = "p1_boost"
	brake = "p1_brake"
	start = "p1_start"
	set_texture(player1_texture)
	set_position(player1_origin)

func player2_init():
	move_left = "p2_left"
	move_right = "p2_right"
	move_up = "p2_up"
	move_down = "p2_down"
	boost = "p2_boost"
	brake = "p2_brake"
	start = "p2_start"
	set_texture(player2_texture)
	set_position(player2_origin)
	
func set_texture(Texture):
	sprite.set_texture(Texture)

func move_player():
		var fx = int(Input.is_action_pressed(move_right)) - int(Input.is_action_pressed(move_left))
		var fy = int(Input.is_action_pressed(move_down)) - int(Input.is_action_pressed(move_up))
		var unit_force = Vector2(fx,fy).normalized()
		
		if Input.is_action_just_pressed(boost):
			unit_force *= 35
		
		if Input.is_action_pressed(brake):
			linear_damp = 7
		else:
			linear_damp = 1
		
		set_applied_force(unit_force * thrust)
		
		if Input.is_action_just_pressed(start):
			get_tree().quit()
		
		rotation = linear_velocity.angle() + PI/2

func _integrate_forces(state):
	if online:
		if is_network_master():
			move_player()
			rpc("updateTrans", state.transform)
			rset("slave_rotation", rotation)
		else:
			if (savedTrans != null):
				state.transform = savedTrans
			rotation = slave_rotation
	else:
		move_player()
		
