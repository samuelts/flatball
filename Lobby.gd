extends Control

var _ip_address = ""

func _on_IPField_text_changed(new_text):
	_ip_address = new_text

func _on_CreateButton_pressed():
	Network.create_server()
	_load_game()

func _on_JoinButton_pressed():
	if _ip_address == "":
		_ip_address = "127.0.0.1"
	Network.connect_to_server(_ip_address)
	_load_game()

func _load_game():
	Network.set_online(true)
	get_tree().change_scene("res://World.tscn")