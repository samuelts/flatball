# Flatball
A competitive multiplayer game similar to air hockey or 2D Rocket League in which players work with (or against) the physics simulation to try and push a ball into the opponent's goal. 

# Current Status
Currently the local multiplayer version of the game functions nearly completely with a working physics simulation, and controls mapped for two game controllers. Networked/online multiplayer is a work in progress, currently lobby system functions and connections between client and server are possible but the physics simulation is non-functioning for slave player objects.

# Controls
Left Analog Stick: Move Player (apply force and rotate with linear velocity)

XBox A: Boost Player (apply single frame large force)

XBox X: Brake Player (increase linear damping while pressed)

# Images
![Menu](https://i.imgur.com/JM8ajv5.png)

![Instructions](https://i.imgur.com/tUfVkO5.png)

![Scores](https://i.imgur.com/TamFls0.png)
