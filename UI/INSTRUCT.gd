extends Label

onready var timer = get_node("./InstructTimer")

func _ready():
	timer.start()
	timer.connect("timeout", self, "queue_free")