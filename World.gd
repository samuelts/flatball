extends Node

var online = Network.online
onready var player = preload("res://PlayerRB.tscn")
onready var ball = preload("res://Ball.tscn")
onready var BallManager = get_node("./BallManager")

func _ready():
	get_tree().connect('network_peer_disconnected', self, '_on_player_disconnected')
	get_tree().connect('server_disconnected', self, '_on_server_disconnected')
	
	pre_configure_game()
	
func _on_player_disconnected(id):
	get_node(str(id)).queue_free()

func _on_server_disconnected():
	get_tree().change_scene("res://Main_Menu.tscn")

remote func pre_configure_game():
	if online:
		var new_player = player.instance()
		new_player.name = str(get_tree().get_network_unique_id())
		new_player.set_network_master(get_tree().get_network_unique_id())
		add_child(new_player)
		var info = Network.self_data
		new_player.init(false)
		
		var new_ball = ball.instance()
		new_ball.name = str(get_tree().get_network_unique_id())
		new_ball.set_network_master(get_tree().get_network_unique_id())
		BallManager.add_child(new_ball)
		new_ball.ball_init()
		
	else:
		print(String(online))
		#Create Player 1
		var Player1 = player.instance()
		add_child(Player1)
		Player1.player1_init()
		
		#Create Player 2
		var Player2 = player.instance()
		add_child(Player2)
		Player2.player2_init()
		
		#Create ball
		var Ball1 = ball.instance()
		BallManager.add_child(Ball1)
		Ball1.ball_init()